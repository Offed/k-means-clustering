﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSI_Zadanie2
{
    public class Centroid : Point
    {
        public List<Point> points;
        private Random rnd;

        public Centroid(double minVal, double maxVal)
        {
            rnd = new Random(System.DateTime.Now.Millisecond);
            x = rnd.Next((int)Math.Floor(minVal), (int)Math.Ceiling(maxVal));
            y = rnd.Next((int)Math.Floor(minVal), (int)Math.Ceiling(maxVal));

            points = new List<Point>();
        }

        public void Reroll(double minVal, double maxVal)
        {
            x = rnd.Next((int)Math.Floor(minVal), (int)Math.Ceiling(maxVal));
            y = rnd.Next((int)Math.Floor(minVal), (int)Math.Ceiling(maxVal));

        }

        public bool RecalculateCoordinates()
        {
            double yMean = 0, xMean = 0;
            for (int i = 0; i < points.Count; i++)
            {
                xMean += points[i].x;
                yMean += points[i].y;
            }
            double xNew = xMean / points.Count;
            double yNew = yMean / points.Count;
            double xOld = x;
            double yOld = y;

            x = xNew;
            y = yNew;

            if (yOld != yNew || xOld != xNew)
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }

        public bool HasPoint(Point point)
        {
            return points.Contains(point);
        }
    }

}
