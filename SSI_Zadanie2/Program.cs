﻿using System;
using System.Collections.Generic;

namespace SSI_Zadanie2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Point> points = new List<Point>();
            points.Add(new Point(1, 3));
            points.Add(new Point(2, 2));
            points.Add(new Point(2, 3));
            
            points.Add(new Point(80, 82));
            points.Add(new Point(77, 77));
            points.Add(new Point(75, 75));
            


            var results = k_meansClustering.clusterPoints(2, points);

            for (int i = 0; i < results.Count; i++)
            {
                Console.WriteLine("Class "+(i+1));
                for (int j = 0; j < results[i].Count; j++)
                {
                    Console.WriteLine(results[i][j].x + " "+ results[i][j].y);
                }
                Console.WriteLine("--------");
            }

            Console.ReadLine();
        }
    }
}
