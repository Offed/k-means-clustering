﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSI_Zadanie2
{
    public class Point
    {
        public double x { get; set; }
        public double y { get; set; }
        public Centroid BelongingTo { get => belongingTo; set => belongingTo = value; }

        private Centroid belongingTo;

        protected Point() { }

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public Point FindClosestPoint(Point[] points)
        {
            Point closestPoint = points[0];

            for (int i = 1; i < points.Length; i++)
            {
                if(DistanceToPoint(points[i]) < DistanceToPoint(closestPoint)){
                    closestPoint = points[i];
                }
            }

            return closestPoint;
        } 

        double DistanceToPoint(Point p)
        {
            return Math.Sqrt(Math.Pow(p.x - x, 2) + Math.Pow(p.y - y, 2));
        }
    }
}
