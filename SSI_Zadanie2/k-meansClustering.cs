﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSI_Zadanie2
{
    public static class k_meansClustering
    {
        public static List<List<Point>> clusterPoints(int k, List<Point> points, double minVal=0, double maxVal=100)
        {

            Centroid[] centroids = new Centroid[k];
            for (int i = 0; i < k; i++)
            {
                centroids[i] = new Centroid(minVal, maxVal);
            }

            int steps = 0;
            int centroidRegenerations = 0;

            bool classesShifted = true;
            while (classesShifted)
            {
                steps++;
                classesShifted = false;
                

                //find closest centroid for each point and re-assign it if needed
                for (int i = 0; i < points.Count; i++)
                {
                    Centroid closestCentroid = (Centroid)points[i].FindClosestPoint(centroids);

                    //remove the point from the previous cluster
                    points[i].BelongingTo?.points.Remove(points[i]);

                    //add to new cluster
                    closestCentroid.points.Add(points[i]);
                    points[i].BelongingTo = closestCentroid;
                }


                //recalculate centroids coordinates and checks for eventual shifts
                for (int i = 0; i < k; i++)
                {
                    if (centroids[i].RecalculateCoordinates())
                    {
                        classesShifted = true;
                    }
                }

                //in case no division was made
                for (int i = 0; i < centroids.Length; i++)
                {
                    if(centroids[i].points.Count == 0)
                    {
                        centroids[i].Reroll(minVal, maxVal);
                        //centroids[i] = new Centroid(minVal,maxVal);
                        centroidRegenerations++;
                    }
                }

            }

            List<List<Point>> abstractionClasses = new List<List<Point>>();

            //divide into abstraction classes

            for (int i = 0; i < centroids.Length; i++)
            {
                abstractionClasses.Add(centroids[i].points);
            }

            Console.WriteLine("K-means steps: " + steps + ", centroid regenerations: " + centroidRegenerations);
            return abstractionClasses;
        }
    }
}
